import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'App de prueba';

  letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

  palabra = 'SUBMARINO';
  palabraOculta = '';
  intentos = 0;
perdio=false;
gano=false;



  constructor() {

    for (let i = 0; i < this.palabra.length; i++) {
      this.palabraOculta = this.palabraOculta + '-';

    }
  }
  comprobar(letra) {
    this.existeLetra(letra);
    let palabraOcultaArr = this.palabraOculta.split('');
    for (let i = 0; i < this.palabra.length; i++) {
      if (letra === this.palabra[i]) {
        palabraOcultaArr[i] = letra;

      }

    }

    this.palabraOculta = palabraOcultaArr.join('');
    this.verificaGana();

  } 
  existeLetra(letra) {
    if (this.palabra.indexOf(letra) >= 0) {

    } else {
      if (this.intentos < 9) {
        this.intentos = this.intentos + 1;
      }

    }

  }
   
  /*
  existeLetra(letra){
    for (let i = 0; i < this.palabra.length; i++) {
      let ltr = this.palabra[i];
      if (ltr==letra){
        return;
      }
      
    
    }
    if(this.intentos<9){
      this.intentos= this.intentos+1;
    }
  
  }*/

  verificaGana(){
    if(this.palabraOculta===this.palabra){
      console.log("Usuario Ganó!!!")
      this.gano=true;
    }
    if(this.intentos>=9){
      console.log("Usuario Perdió!!!");
      this.perdio=true;

    }
  }

}
